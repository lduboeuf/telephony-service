# Malay translation for telephony-service
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the telephony-service package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: telephony-service\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-15 21:05+0000\n"
"PO-Revision-Date: 2014-10-22 09:26+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Malay <ms@li.org>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2017-01-04 06:02+0000\n"
"X-Generator: Launchpad (build 18298)\n"

#: indicator/messagingmenu.cpp:373
#, qt-format
msgid "%1 missed call"
msgid_plural "%1 missed calls"
msgstr[0] "%1 panggilan terlepas"
msgstr[1] "%1 panggilan terlepas"

#: indicator/messagingmenu.cpp:459
#, qt-format
msgid "%1 voicemail message"
msgid_plural "%1 voicemail messages"
msgstr[0] "%1 panggilan mel bersuara"
msgstr[1] "%1 panggilan mel bersuara"

#: indicator/metrics.cpp:48
#, qt-format
msgid "<b>%1</b> calls made today"
msgstr "Panggilan <b>%1</b> dibuat hari ini"

#: indicator/metrics.cpp:46
#, qt-format
msgid "<b>%1</b> calls received today"
msgstr "Panggilan <b>%1</b> diterima hari ini"

#: indicator/metrics.cpp:44
#, qt-format
msgid "<b>%1</b> text messages received today"
msgstr "Mesej teks <b>%1</b> diterima hari ini"

#: indicator/metrics.cpp:42
#, qt-format
msgid "<b>%1</b> text messages sent today"
msgstr "Mesej teks <b>%1</b> dihantar hari ini"

#: approver/approver.cpp:523
msgid "Accept"
msgstr "Terima"

#: indicator/textchannelobserver.cpp:499
#, qt-format
msgid "Attachment: %1 audio clip"
msgid_plural "Attachments: %1 audio clips"
msgstr[0] "Lampiran: %1 klip audio"
msgstr[1] "Lampiran: %1 klip audio"

#: indicator/textchannelobserver.cpp:497
#, qt-format
msgid "Attachment: %1 contact"
msgid_plural "Attachments: %1 contacts"
msgstr[0] "Lampiran: %1 kenalan"
msgstr[1] "Lampiran: %1 kenalan"

#: indicator/textchannelobserver.cpp:501
#, qt-format
msgid "Attachment: %1 file"
msgid_plural "Attachments: %1 files"
msgstr[0] "Lampiran: %1 fail"
msgstr[1] "Lampiran: %1 fail"

#: indicator/textchannelobserver.cpp:493
#, qt-format
msgid "Attachment: %1 image"
msgid_plural "Attachments: %1 images"
msgstr[0] "Lampiran: %1 imej"
msgstr[1] "Lampiran: %1 imej"

#: indicator/textchannelobserver.cpp:495
#, qt-format
msgid "Attachment: %1 video"
msgid_plural "Attachments: %1 videos"
msgstr[0] "Lampiran: %1 video"
msgstr[1] "Lampiran: %1 video"

#: indicator/authhandler.cpp:72
msgid "Authentication failed. Do you want to review your credentials?"
msgstr "Pengesahihan gagal. Anda mahu kaji semula kelayakan anda?"

#: indicator/messagingmenu.cpp:309
msgid "Call back"
msgstr "Panggilan semula"

#: approver/approver.cpp:469 approver/approver.cpp:484
msgid "Caller number is not available"
msgstr "Nombor pemanggil tidak tersedia"

#: approver/approver.cpp:481
#, qt-format
msgid "Calling from %1"
msgstr "Panggilan daripada %1"

#: approver/approver.cpp:475
msgid "Calling from private number"
msgstr "Panggilan dari nombor persendirian"

#: approver/approver.cpp:478
msgid "Calling from unknown number"
msgstr "Panggilan dari nombor tidak diketahui"

#: indicator/ussdindicator.cpp:142
msgid "Cancel"
msgstr "Batal"

#: indicator/textchannelobserver.cpp:225
msgid "Deactivate flight mode and try again from the messaging application."
msgstr "Nyahaktifkan mod penerbangan dan cuba lagi dari aplikasi pemesejan."

#: approver/approver.cpp:545
msgid "Decline"
msgstr "Tolak"

#: approver/approver.cpp:536
msgid "End + Answer"
msgstr "Tamat + Jawab"

#: approver/approver.cpp:522
msgid "Hold + Answer"
msgstr "Tangguh + Jawab"

#: indicator/messagingmenu.cpp:314
msgid "I missed your call - can you call me now?"
msgstr "Saya terlepas panggilan anda - boleh anda telefon saya sekarang?"

#: indicator/messagingmenu.cpp:317
msgid "I'll be 20 minutes late."
msgstr "Saya akan terlambat 20 minit."

#: approver/approver.cpp:93
msgid "I'm busy at the moment. I'll call later."
msgstr "Saya sibuk pada masa ini. Saya akan menghubungi kemudian."

#: indicator/messagingmenu.cpp:316
msgid "I'm busy at the moment. I'll call you later."
msgstr "Saya sibuk buat masa ini. Saya akan telefon anda kemudian."

#: approver/approver.cpp:94
msgid "I'm running late, on my way now."
msgstr "Saya terlambat, dalam perjalanan sekarang."

#: indicator/messagingmenu.cpp:315
msgid "I'm running late. I'm on my way."
msgstr "Saya terlewat. Saya masih dalam perjalanan."

#: approver/approver.cpp:553
msgid "Message & decline"
msgstr "Mesej & abai"

#: indicator/textchannelobserver.cpp:638
#, qt-format
msgid "Message from %1"
msgstr "Mesej daripada %1"

#. TRANSLATORS : %1 is the group name and %2 is the recipient name
#: indicator/messagingmenu.cpp:226 indicator/textchannelobserver.cpp:560
#: indicator/textchannelobserver.cpp:564
#, qt-format
msgid "Message to %1 from %2"
msgstr ""

#. TRANSLATORS : %1 is the recipient name
#: indicator/messagingmenu.cpp:229 indicator/messagingmenu.cpp:233
#: indicator/textchannelobserver.cpp:568 indicator/textchannelobserver.cpp:572
#, qt-format
msgid "Message to group from %1"
msgstr "Mesej ke kumpulan daripada %1"

#: indicator/textchannelobserver.cpp:293
msgid "New Group"
msgstr ""

#. TRANSLATORS : message displayed when any error occurred while receiving a MMS (case when cellular-data is off, or any downloading issue). Notify that there was a message, the user can find more about it in the messaging-app.
#: indicator/textchannelobserver.cpp:504
#, fuzzy
#| msgid "View message"
msgid "New MMS message"
msgstr "Lihat mesej"

#: indicator/authhandler.cpp:94
msgid "No"
msgstr "Tidak"

#: indicator/metrics.cpp:49 indicator/metrics.cpp:51
msgid "No calls made today"
msgstr "Tiada panggilan dibuat hari ini"

#: indicator/metrics.cpp:47
msgid "No calls received today"
msgstr "Tiada panggilan diterima hari ini"

#: indicator/metrics.cpp:45
msgid "No text messages received today"
msgstr "Tiada mesej teks diterima hari ini"

#: indicator/metrics.cpp:43
msgid "No text messages sent today"
msgstr "Tiada mesej teks dihantar hari ini"

#: indicator/ussdindicator.cpp:116 indicator/textchannelobserver.cpp:365
msgid "Ok"
msgstr "Ok"

#: approver/approver.cpp:456
#, qt-format
msgid "On [%1]"
msgstr "Pada [%1]"

#: indicator/telephony-service-call.desktop.in:3
msgid "Phone Calls"
msgstr "Panggilan Telefon"

#: approver/approver.cpp:95
msgid "Please call me back later."
msgstr "Sila hubungi saya semula."

#: indicator/textchannelobserver.cpp:793
msgid "Please, select a SIM card:"
msgstr "Sila, pilih satu kad SIM:"

#: approver/approver.cpp:460 indicator/messagingmenu.cpp:378
msgid "Private number"
msgstr "Nombor persendirian"

#: indicator/ussdindicator.cpp:119
msgid "Reply"
msgstr "Balas"

#: indicator/displaynamesettings.cpp:35
#, qt-format
msgid "SIM %1"
msgstr "SIM %1"

#: indicator/telephony-service-sms.desktop.in:3
msgid "SMS"
msgstr "SMS"

#: indicator/textchannelobserver.cpp:371
msgid "Save"
msgstr "Simpan"

#: indicator/messagingmenu.cpp:151 indicator/messagingmenu.cpp:257
#: indicator/messagingmenu.cpp:324
msgid "Send"
msgstr "Hantar"

#: indicator/messagingmenu.cpp:318
msgid "Sorry, I'm still busy. I'll call you later."
msgstr "Maaf, Saya masih sibuk. Saya akan telefon anda nanti."

#: indicator/metrics.cpp:50
#, qt-format
msgid "Spent <b>%1</b> minutes in calls today"
msgstr "Guna sebanyak <b>%1</b> miniy panggilan hari ini"

#: indicator/messagingmenu.cpp:83 indicator/messagingmenu.cpp:87
msgid "Telephony Service"
msgstr "Perkhidmatan Telefoni"

#: approver/main.cpp:46
msgid "Telephony Service Approver"
msgstr "Approver Perkhidmatan Telefoni"

#: indicator/main.cpp:53
msgid "Telephony Service Indicator"
msgstr "Penunjuk Perkhidmatan Telefoni"

#: indicator/textchannelobserver.cpp:234
msgid "The message could not be sent"
msgstr "Mesej tidak dapat dihantar"

#: indicator/textchannelobserver.cpp:228
msgid "Try again from the messaging application."
msgstr "Cuba lagi menerusi aplikasi pemesejan."

#: approver/approver.cpp:65
msgid "Unknown caller"
msgstr "Pemanggil tidak diketahui"

#: approver/approver.cpp:463 indicator/messagingmenu.cpp:208
#: indicator/messagingmenu.cpp:382 indicator/textchannelobserver.cpp:529
msgid "Unknown number"
msgstr "Nombor tidak diketahui"

#: indicator/textchannelobserver.cpp:223
msgid "Unlock your sim card and try again from the messaging application."
msgstr "Nyahsekat kad sim anda dan cuba lagi melalui aplikasi pemesejan."

#: indicator/textchannelobserver.cpp:326
msgid "View Group"
msgstr ""

#: indicator/textchannelobserver.cpp:247 indicator/textchannelobserver.cpp:602
msgid "View message"
msgstr "Lihat mesej"

#: indicator/messagingmenu.cpp:464
msgid "Voicemail"
msgstr "Mel bersuara"

#: indicator/messagingmenu.cpp:456
msgid "Voicemail messages"
msgstr "Mesej mel bersuara"

#: indicator/authhandler.cpp:91
msgid "Yes"
msgstr "Ya"

#: indicator/textchannelobserver.cpp:307
msgid "You joined a new group"
msgstr ""

#. TRANSLATORS : %1 is the group name
#: indicator/textchannelobserver.cpp:304
#, qt-format
msgid "You joined group %1"
msgstr ""

#. TRANSLATORS : %1 is the group name
#: indicator/textchannelobserver.cpp:300
#, qt-format
msgid "You joined group %1 "
msgstr ""

#~ msgid "Private Number"
#~ msgstr "Nombor Persendirian"

#~ msgid "Unknown Number"
#~ msgstr "Nombor Tidak Diketahui"
